# OpenML dataset: Radar-Traffic-Data

https://www.openml.org/d/43354

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Traffic data collected from the several Wavetronix radar sensors deployed by the City of Austin. Dataset is augmented with geo coordinates from sensor location dataset.
Source: https://data.austintexas.gov/
Content
What's inside is more than just rows and columns. Make it easy for others to get started by describing how you acquired the data and what time period it represents, too.
Acknowledgements
Data Source: https://data.austintexas.gov/
Photo by Jeremy Banks on Unsplash
Inspiration
Your data will be in front of the world's largest data science community. What questions do you want to see answered?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43354) of an [OpenML dataset](https://www.openml.org/d/43354). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43354/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43354/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43354/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

